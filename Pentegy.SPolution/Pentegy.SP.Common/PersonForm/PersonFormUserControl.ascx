﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PersonFormUserControl.ascx.cs" Inherits="Pentegy.SP.Common.PersonForm.PersonFormUserControl" %>
<h1>Dodawanie użytkownika</h1>
<p>Dodaj dane nowego użytkownika</p>

<table>
    <tr style="margin:10px;">
        <td>Name: </td>
        <td>
            <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr></tr>
     <tr style="margin:10px;">
        <td>Lastname: </td>
        <td>
            <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
        </td>
    </tr>
     <tr style="margin:10px;">
        <td>Phone: </td>
        <td>
            <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
        </td>
    </tr>
        <tr style="margin:10px;">
        <td>Adress: </td>
        <td>
            <asp:TextBox ID="txtAdress" runat="server"></asp:TextBox>
        </td>
    </tr>

    
</table>
<div >
<%--<form id="upload-widget" method="post" class="dropzone">
     <input name="file" type="file" ID="ImageFile" />
</form>--%>
  <asp:FileUpload ID="FileUpload" runat="server" />
    </div>
<table>
    <tr>
        <td>
       <asp:Button ID="BtnSave" runat="server" Text="Add" OnClick="BtnSave_Click" />
            </td>
        </tr>
    </table>
 