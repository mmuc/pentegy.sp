﻿using Microsoft.SharePoint;
using Pentegy.SP.CustomListClass;
using SharepointCommon;
using System;
using System.Runtime.InteropServices.ComTypes;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace Pentegy.SP.Common.PersonForm
{
    public partial class PersonFormUserControl : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }


        protected void BtnSave_Click(object sender, EventArgs e)
        {
            // Image img = upload
            FileUpload elem = FileUpload;

            if (elem.HasFile)
            {
                
                using (var factory = WebFactory.Open(Common.configListUrl))
                {

                    var lib = factory.GetByUrl<Document>(Common.AccountsPictures);
                  

                    var document = new Document
                    {
                        Name = elem.FileName,
                        Content = elem.FileBytes,
                        RenameIfExists = true,
                    };

                    lib.Add(document); 
                }
        
            }
             

                ExtUser test = new ExtUser(Convert.ToString(txtName.Text), Convert.ToString(txtLastName.Text), Convert.ToString(txtPhone.Text), Convert.ToString(txtAdress.Text));

            using (var factory = WebFactory.Open("http://tst-sp2016/sites/TestWhoIsWho/"))
            {
                var list = factory.GetByName<Item>("PersonList");

             
                list.Add(test);

                var items = list.Items();
            }
        }
    }
}
