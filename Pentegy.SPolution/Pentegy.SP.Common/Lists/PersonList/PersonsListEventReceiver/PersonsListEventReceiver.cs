﻿using System;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.Workflow;

namespace Pentegy.SP.Common.Lists.PersonList.PersonsListEventReceiver
{
    /// <summary>
    /// List Item Events
    /// </summary>
    public class PersonsListEventReceiver : SPItemEventReceiver
    {
        /// <summary>
        /// An item is being added.
        /// </summary>
        public override void ItemAdding(SPItemEventProperties properties)
        {
            base.ItemAdding(properties);


            string itemName = properties.AfterProperties["FirstName"].ToString();
            string itemLastName = properties.AfterProperties["LastName"].ToString();

            properties.AfterProperties["Title"] = itemName+"_"+itemLastName;

        //    properties.ListItem.Update();
          
        }

        /// <summary>
        /// An item is being updated.
        /// </summary>
        public override void ItemUpdating(SPItemEventProperties properties)
        {
            base.ItemUpdating(properties);
        }


    }
}