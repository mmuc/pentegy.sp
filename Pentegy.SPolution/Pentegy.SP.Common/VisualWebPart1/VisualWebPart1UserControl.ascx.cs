﻿using SharepointCommon;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace Pentegy.SP.Common.VisualWebPart1
{
    public partial class VisualWebPart1UserControl : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnDoTheStuff_Click(object sender, EventArgs e)
        {
            using (var factory = WebFactory.Open("http://tst-sp2016/sites/TestWhoIsWho/"))
            {
                var list = factory.GetByName<Item>("Config");
                //var listByUrl = factory.GetByUrl<Item>("lists/list1");
                //var listById = factory.GetById<Item>(new Guid("guid-here"));

                var lookupItem = new Item { Title = "item1" };
                list.Add(lookupItem);

              //  var customItem = new
                //{
                //    Title = "TestTitle",
                //    CustomField1 = "Field1",
                //    CustomFieldNumber = 123.5,
                //    CustomBoolean = true,
                //    CustomUser = new Person("DOMAIN\USER1"),
                //    CustomUsers = new { new Person("DOMAIN\USER1") },
                //    CustomLookup = lookupItem,
                //    CustomMultiLookup = new List<Item> { lookupItem, lookupItem2 },
                //    CustomDate = DateTime.Now,
                //};
                //list.Add(customItem);
            }

            lblValue.Text = Common.GetProperty(Convert.ToString(tbxPropertyName.Text));
        }

        protected void setNamebtn_Click(object sender, EventArgs e)
        {
                if (Common.SetSearchedListName(Convert.ToString(txtSetName.Text)))
            {
                lblNameValue.Text = "Zmieniono nazwe listy na " + txtSetName.Text;
            }
            else
            {
                lblNameValue.Text = "Nie znaleziono listy " + txtSetName.Text;
            }
        }


        protected void setUrlbtn_Click(object sender, EventArgs e)
        {
            if (Common.SetSearchedListUrl(Convert.ToString(txtSetUrl.Text)))
            {
                lblUrlValue.Text = "Zmieniono adres list na " + txtSetUrl.Text;
            }
            else
            {
                lblUrlValue.Text = "Nie znaleziono strony " + txtSetUrl.Text;
            }
        }




        


    }
}
