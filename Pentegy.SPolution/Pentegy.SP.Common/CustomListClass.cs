﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint.Client;
using System.ComponentModel.DataAnnotations;
using Microsoft.SharePoint.Mobile.Controls;
using SharepointCommon;
using SharepointCommon.Attributes;

namespace Pentegy.SP.CustomListClass
{
    public class ExtUser : Item
    {
        [Required]
        [Field(Name = "FirstName", DisplayName = "Kopytko")]
        public virtual string Name { get; set; }  //Text


        [Required]
        [Field(Name ="LastName",DisplayName ="Costam")]
        public virtual string LastName { get; set; }

        [Required, StringLength(12, ErrorMessage = "Not valid phone number."), RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "Not valid phone number.")]
        [Field(Name = "WorkPhone",DisplayName ="Tel")]
        public virtual string PhoneNumber { get; set; }

        [Required]
        [Field(Name = "WorkAddress",DisplayName ="Adres")]
        public virtual string Adress { get; set; }

        [Field(Name = "AccountPicture",DisplayName ="CostamFoto")]

        public virtual Image Img { get; set; }







        public ExtUser(string nameTmp, string lastNameTmp, string phoneNumberTmp, string adressTmp)
        {      
            Name = nameTmp;
            LastName = lastNameTmp;
            PhoneNumber = phoneNumberTmp;
            Adress = adressTmp;
           // Img = imgtmp;
        }

    }


}