﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint.Client;

namespace Pentegy.SP.Common
{
    public static class Common
    {       
        public static string configListUrl = "http://tst-sp2016/sites/TestWhoIsWho/";

        public static string AccountsPictures = "Lists/AccountsImagesLibrary";

        public static string listName;

        public static string searchedFieldName;
            

        public static bool SetSearchedListName(string name)
        {
       
            using (SPSite oPsite = new SPSite(configListUrl))

            {
                using (SPWeb oSPWeb = oPsite.OpenWeb())
                {
                    SPList configList = null;
                    try
                    {
                        configList = oSPWeb.Lists[name];
                        listName = name;
                        return true;
              
                    }
                    catch(Exception ex)
                    {
                        SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Missing List", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                    }
                }

            }

            return false;
        }

     

        public static bool SetSearchedListUrl(string url)
        {
            try
            {
                using (SPSite oPsite = new SPSite(url))
                {
                    using (SPWeb web = oPsite.OpenWeb(url, true))
                    {
                        configListUrl = url;
                        return true;
                    }
                }
            }
            catch(Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Site Missing", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            }
            return false;
        }
       
        public static string GetProperty(string propertyName)
        {
            string propertyValue = null;

            using (SPSite oPsite = new SPSite(configListUrl))

            {
                using (SPWeb oSPWeb = oPsite.OpenWeb())
                {

                    SPList configList = null;

                    if (listName == null)
                        listName = "Config";

                    try
                    {
                        configList = oSPWeb.Lists[listName];

                        //SPListItem = ConfigList.GetItemByIdSelectedFields()

                        SPQuery query = new SPQuery();
                        query.Query = String.Format("<Query><Where><Eq><FieldRef Name='Title'/><Value type='string'>{0}</value></Eq></Where></Query>", propertyName);

                        SPListItemCollection items = configList.GetItems(query);

                        if (items.Count != 0)
                            propertyValue = items[0]["Value"].ToString();

                        else
                            propertyValue = "Item not found";
                    }

                    catch( Exception ex)
                    {
                        SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Config List Missing", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                    }
                }

            }             

            return propertyValue;
        }
    }
}
