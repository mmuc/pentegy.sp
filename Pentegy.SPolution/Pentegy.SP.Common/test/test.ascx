﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="test.ascx.cs" Inherits="Pentegy.SP.Common.test.test" %>
<h1>test super formularza</h1>
<p>Super formularz do dodawania nowych pracownikow</p>

<table>
    <tr>
        <td>Imie</td>
        <td>
            <asp:TextBox ID="txtImie" runat="server"></asp:TextBox>
        </td>
    </tr>
     <tr>
        <td>Tytul</td>
        <td>
            <asp:TextBox ID="txtTytul" runat="server"></asp:TextBox>
        </td>
    </tr>
     <tr>
        <td>Login</td>
        <td>
            <asp:TextBox ID="txtLogin" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <asp:Button ID="BtnSave" runat="server" Text="Zapisz" OnClick="BtnSave_Click" />
    </tr>
</table>